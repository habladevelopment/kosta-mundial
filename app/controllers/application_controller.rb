class ApplicationController < ActionController::Base
  def index
  end
  def register
    @contact = Contact.new
    code = params["code"]
    unless code.blank?
      session[:code] = nil
    end
    if session[:code].blank?
      if valid_code code
        session[:code] = params["code"]
      else
        flash[:alert] = "Tu código no existe o ya fue redimido, intenta de nuevo"
        redirect_to root_path("#registro")
      end
    end
  end

  def new_register
    codes = params["code_invoice"]
    invalids = 0
    code_invalids = ""

    cookies[:name] = params[:contact][:name]
    cookies[:email] = params[:contact][:email]
    cookies[:doc_type] = params[:contact][:doc_type]
    cookies[:doc_number] = params[:contact][:doc_number]
    cookies[:phone] = params[:contact][:phone]
    cookies[:city] = params[:contact][:city]
    cookies[:profesion] = params[:contact][:profesion]

    codes.each do |code|
      unless code.blank?
        if valid_code code
          @contact = Contact.new(contact_params)
          if @contact.valid?
            @contact.code_invoice = code
            if @contact.save
              CodeInvoice.find_by_code(code).update(redeemed: true)
            end
            #UserMailer.contact_us(@contact).deliver_now
            UserMailer.contact_user(@contact).deliver_now
          end
        else
          invalids = invalids + 1
          if code_invalids == ''
            code_invalids = code
          else
            code_invalids = "#{code_invalids}, #{code} "
          end
         end
      end
    end
    if invalids > 0
      flash[:alert] = "#{invalids} de tus código no existen: #{code_invalids}"
    end
    redirect_to thank_you_path
  end


  private

  def valid_code code
    code_r = CodeInvoice.find_by_code(code)
    if code_r.nil?
      false
    else
      code_r.redeemed.blank?
    end

  end
  # Use callbacks to share common setup or constraints between actions.
  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require(:contact).permit(:name, :email, :doc_type, :doc_number, :phone, :city, :profesion, :tienda)
  end
end

