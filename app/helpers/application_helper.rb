module ApplicationHelper
  def custom_bootstrap_flash
    flash_messages = []
    flash.each do |type, message|
      type = 'success' if type == 'notice'
      type = 'error'   if type == 'alert'
      text = "<script>toastr.#{type}('#{message}');</script>"
      flash_messages << text.html_safe if message
    end
    flash_messages.join("\n").html_safe
  end

  def tiendas
    [
        ["C.C Unicentro Bogotá", "C.C Unicentro Bogotá"],
        ["C.C Plaza de las Américas", "C.C Plaza de las Américas"],
        ["Almacén de Fábrica Bogotá (Corferias)", "Almacén de Fábrica Bogotá (Corferias)"],
        ["C.C Salitre", "C.C Salitre"],
        ["C.C Metrópolis", "C.C Metrópolis"],
        ["C.C Bulevar", "C.C Bulevar"],
        ["C.C Cafam", "C.C Cafam"],
        ["C.C Santafé", "C.C Santafé"],
        ["Cra 7 Centro Bogotá", "Cra 7 Centro Bogotá"],
        ["C.C Ventura Terreros Soacha", "C.C Ventura Terreros Soacha"],
        ["C.C. Unicentro Cali", "C.C. Unicentro Cali"],
        ["C.C Cosmocentro", "C.C Cosmocentro"],
        ["Centro Palmira", "Centro Palmira"],
        ["C.C Llanogrande Plaza", "C.C Llanogrande Plaza"],
        ["C.C Unicentro Palmira", "C.C Unicentro Palmira"],
        ["C.C. La Herradura", "C.C. La Herradura"],
        ["C.C Unicentro Pasto", "C.C Unicentro Pasto"],
        ["Almacén de Fábrica Dosquebradas", "Almacén de Fábrica Dosquebradas"]
    ]
  end
end
