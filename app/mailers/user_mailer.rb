class UserMailer < ApplicationMailer

  def contact_us(user)
		@user = user
		mail(to: "ti2@hablacreativo.com" , subject: 'Contacto desde el sitio web')
		# mail(to: [ENV['EMAIL_TO_CONTACT_US'].split(",").map(&:strip)], subject: 'Contacto desde el sitio web')
	end

   def contact_user(user)
 		@user = user
 		mail(to: @user.email, subject: '!Gracias por comunicarte con nosotros!')
    # mail(to: 'ti@hablacreativo.com', subject: '')
 	 end
end
