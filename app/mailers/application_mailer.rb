class ApplicationMailer < ActionMailer::Base
  default from: 'info@kostazul.com'
  layout 'mailer'
end
