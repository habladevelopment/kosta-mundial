class CodeInvoice < ApplicationRecord
  def self.call
    require 'smarter_csv'
    SmarterCSV.process('codes.csv') do |chunk|
      chunk.each do |data_hash|
        CodeInvoice.create!( data_hash )
      end
    end
  end
end
