class CreateCodeInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :code_invoices do |t|
      t.string :code
      t.boolean :redeemed, default: false

      t.timestamps
    end
  end
end
