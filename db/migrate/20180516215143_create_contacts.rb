class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :email
      t.string :doc_type
      t.string :doc_number
      t.string :phone
      t.string :city
      t.string :profesion
      t.string :tienda
      t.string :code_invoice

      t.timestamps
    end
  end
end
