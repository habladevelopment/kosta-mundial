Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'application#index'

  get '/registrar', to: 'application#register', as: "register"
  post '/registrar', to: 'application#register', as: "register_code"
  get '/registro_codigo', to: 'application#new_register'
  post '/registro_codigo', to: 'application#new_register', as: "new_register_code"
  get '/gracias', to: 'application#thank_you', as: "thank_you"
end
